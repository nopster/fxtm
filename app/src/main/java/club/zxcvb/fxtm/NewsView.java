package club.zxcvb.fxtm;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

/**
 * Created by MikeSimons on 09.06.2015.
 * News viewer
 */
public class NewsView extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.news_view);
        Bundle extras = getIntent().getExtras();
        int pos = -1;
        if(extras == null) {
            pos= -1;
        } else {
            pos= extras.getInt("selectedIndex");
        }
        WebView webView = ((WebView) findViewById(R.id.newswebview));
        webView.setWebViewClient(new WebViewClient() {
            // Override URL
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                return false;
            }
        });
        webView.loadData("<b>" + NewsList.getInstance().getNews(pos).title+"</b></br>"+
                        "<u>"+NewsList.getInstance().getNews(pos).pubDate+"</u><br>"+
                        NewsList.getInstance().getNews(pos).text, "text/html", "utf-8");
    }
}
