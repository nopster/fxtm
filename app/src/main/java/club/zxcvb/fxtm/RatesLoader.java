package club.zxcvb.fxtm;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by MikeSimons on 09.06.2015.
 * Retrieve rates from fxtm api
 */
public class RatesLoader extends AsyncTask<Void, Void, String> {
    public Activity activity;
    public AsyncResponseRates delegate=null;

    @Override
    protected void onPreExecute() {
            super.onPreExecute();
            }
    HttpURLConnection urlConnection = null;
    BufferedReader reader = null;
    String resultJson = "";

    /**
     * Get background rates
     * @param params
     * @return
     */
    @Override
    protected String doInBackground(Void... params) {
            try{
                URL url = new URL("http://www.forextime.com/informers/rates/full");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                resultJson = buffer.toString();
            }
            catch (Exception e) {
              e.printStackTrace();
            }
            return null;
    }

    /**
     * Send to mainActivity result json
     * @param result
     */
    @Override
    protected void onPostExecute(String result) {
            super.onPostExecute(result);
            delegate.processFinish2(resultJson);
    }
}