package club.zxcvb.fxtm;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.renderscript.Element;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * MainActivity.java
 * Main screen
 */
public class MainActivity extends ActionBarActivity implements  AsyncResponse,AsyncResponseRates {


    ProgressDialog progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TabHost tabs = (TabHost) findViewById(android.R.id.tabhost);
        tabs.setup();
        TabHost.TabSpec spec = tabs.newTabSpec("tag1");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Rates");
        tabs.addTab(spec);
        spec = tabs.newTabSpec("tag2");
        spec.setContent(R.id.tab2);
        spec.setIndicator("News");
        tabs.addTab(spec);
        tabs.setCurrentTab(0);

        ExcuteNetworkOperation excuteNetworkOperation = new ExcuteNetworkOperation();
        excuteNetworkOperation.delegate=this;
        excuteNetworkOperation.execute();

        RatesLoader ratesLoader = new RatesLoader();
        ratesLoader.delegate=this;
        ratesLoader.execute();

        progress = new ProgressDialog(this);
        progress.setMessage("Please wait...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.show();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Parse rates
     * @param output JSON string with rates
     */
    public void processFinish2(String output) {
        try {
            progress.dismiss();
            JSONArray jArray = new JSONArray(output);
            for(int i=0;i<jArray.length(); i++) {
                JSONObject json_data = jArray.getJSONObject(i);
                String symbol = json_data.getString("symbol");
                String ask = json_data.getString("ask");
                String bid = json_data.getString("bid");
                addRow(symbol, ask, bid);
            }
            }
        catch (Exception e){
            Toast.makeText(this,"Parse error!",Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Add rate to table
     * @param symbol
     * @param bid
     * @param ask
     */
    public void addRow(String symbol, String bid, String ask) {
        TableLayout tableLayout = (TableLayout) findViewById(R.id.table);
        LayoutInflater inflater = LayoutInflater.from(this);
        TableRow tr = (TableRow) inflater.inflate(R.layout.table_row, null);
        TextView tv = (TextView) tr.findViewById(R.id.col1);
        tv.setText(symbol);
        tv = (TextView) tr.findViewById(R.id.col2);
        tv.setText(bid);
        tv = (TextView) tr.findViewById(R.id.col3);
        tv.setText(ask);
        tableLayout.addView(tr);
    }

    /**
     * Parse news
     * @param output array of news title
     */
    public void processFinish(ArrayList<String> output){
        if (output == null)
            return;

        ListView lvMain = (ListView) findViewById(R.id.lvMain);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, output);

        lvMain.setAdapter(adapter);
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                intent = new Intent(getBaseContext(), NewsView.class);
                intent.putExtra("selectedIndex", position);
                startActivity(intent);
            }
        });
    }
}
