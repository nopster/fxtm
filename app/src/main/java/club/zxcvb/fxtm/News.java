package club.zxcvb.fxtm;

/**
 * Created by MikeSimons on 09.06.2015.
 * Single news
 */
public class News {
    public String title;
    public String text;
    public String pubDate;

    /**
     * Constructor news
     * @param title
     * @param text
     * @param pubDate
     */
    public News(String title, String text, String pubDate) {
        this.title = title;
        this.text = text;
        this.pubDate = pubDate;
    }
}
