package club.zxcvb.fxtm;

import java.util.ArrayList;

/**
 * Created by MikeSimons on 09.06.2015.
 *  News list handler
 */
public class NewsList {
    private static NewsList ourInstance = new NewsList();
    private ArrayList<News> newses = new ArrayList<News>();
    public static NewsList getInstance() {
        return ourInstance;
    }

    /**
     * Add news to news list
     * @param title news title
     * @param text news description
     * @param pubDate news publicatio date
     */
    public void addNews(String title,String text, String pubDate){
        News news = new News(title,text,pubDate);
        newses.add(news);
    }

    /**
     * Get news by id
     * @param i news id
     * @return News
     */
    public News getNews(int i){
        return newses.get(i);
    }
    private NewsList() {
    }
}
