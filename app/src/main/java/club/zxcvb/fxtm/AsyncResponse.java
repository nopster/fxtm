package club.zxcvb.fxtm;

import java.util.ArrayList;

/**
 * Created by MikeSimons on 09.06.2015.
 */
public interface AsyncResponse  {
    void processFinish(ArrayList<String> output);
}
