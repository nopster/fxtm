package club.zxcvb.fxtm;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by MikeSimons on 08.06.2015.
 * Retrieve async forex news
 */
public class ExcuteNetworkOperation extends AsyncTask<Void, Void, ArrayList<String>> {
    public Activity activity;
    public AsyncResponse delegate=null;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
    ArrayList<String> items = new ArrayList<String>();


    @Override
    protected ArrayList<String> doInBackground(Void... params) {


        try{
            URL url = new URL("http://www.forextime.com/news.rss?lang=en");
            URLConnection conn = url.openConnection();

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(conn.getInputStream());

            NodeList nodes = doc.getElementsByTagName("item");

            for (int i = 0; i < nodes.getLength(); i++) {
                org.w3c.dom.Element element = (org.w3c.dom.Element) nodes.item(i);
                if (element.getElementsByTagName("title")==null) continue;
                NodeList title = element.getElementsByTagName("title");
                org.w3c.dom.Element vTitle = (org.w3c.dom.Element) title.item(0);

                NodeList description = element.getElementsByTagName("description");
                org.w3c.dom.Element vDescription = (org.w3c.dom.Element) description.item(0);

                NodeList pubDate = element.getElementsByTagName("pubDate");
                org.w3c.dom.Element vpubDate = (org.w3c.dom.Element) pubDate.item(0);

                NewsList.getInstance().addNews(vTitle.getTextContent(),vDescription.getTextContent(),vpubDate.getTextContent());
                items.add(vTitle.getTextContent());

            }

            }
        catch (Exception e) {
                e.printStackTrace();
            }
        return null;
    }
    @Override
    protected void onPostExecute(ArrayList<String> result) {
        super.onPostExecute(result);
        delegate.processFinish(items);
    }
}